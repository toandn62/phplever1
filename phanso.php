<?php
	class Phanso {
		public $TuSo;
		public $MauSo;
		// hàm tạo
		public function __construct($tuso, $mauso) {
			$this->TuSo 	= $tuso;
			$this->MauSo	= $mauso;
		}
		// in
		public function HienThi() {
			echo $this->TuSo.' / '.$this->MauSo;
		}
		// rút gọn cách 1
		public function RutGonPs1() {
			$ucln = $this->UCLN($this->TuSo, $this->MauSo);
			$this->TuSo = $this->TuSo/$ucln;
			$this->MauSo = $this->MauSo/$ucln;
			echo $this->TuSo.' / '.$this->MauSo;
		}
		// rút gọn cách 2
		public function RutGonPs2() {
			$ucln = $this->UCLN2($this->TuSo, $this->MauSo);
			$this->TuSo = $this->TuSo/$ucln;
			$this->MauSo = $this->MauSo/$ucln;
		}
		// Tìm ucln c1
		private function UCLN($a, $b) {
			$min = min(array($a, $b));
			while($min > 0) {
				if($a % $min == 0 && $b % $min == 0) return $min;
				$min --;
			}
		}
		// Tìm ucln c2
		private function UCLN2($a, $b) {
			while($a != $b) {
				if($a > $b) {
					$a = $a - $b;
				}
				else $b = $b - $a;
			}
			return $a;
		}
		// cộng
		public function Cong($phanso) {
			$this->TuSo = $this->TuSo * $phanso->MauSo + $phanso->TuSo * $this->MauSo;
			$this->MauSo = $this->MauSo * $phanso->MauSo;
			$this->RutGonPs2();
		}
		// trừ
		public function Tru($phanso) {
			$this->TuSo = $this->TuSo * $phanso->MauSo - $phanso->TuSo * $this->MauSo;
			$this->MauSo = $this->MauSo * $phanso->MauSo;
			$this->RutGonPs2();
		}
		// Nhân
		public function Nhan() {
			
		}
		// Chia
		public function Chia() {
			
		}
		
	}
	
	$Test 	= new Phanso(2, 4);
	$Test2 	= new Phanso(25, 5);
	echo "ps1: --------- ";
	$Test->HienThi();
	echo "<br>";
	echo "ps1 rút gọn: ";
	$Test->RutGonPs1();
	echo "<br>";
	echo "ps2: --------- ";
	$Test2->HienThi();
	echo "<br>";
	echo "ps2 rút gọn: ";
	$Test2->RutGonPs1();
	echo "<br>";
	echo "tổng 2 ps: ";
	$Test->Cong($Test2);
	$Test->HienThi();
	echo "<br>";
	echo "hiệu 2 ps: ";
	$Test->Tru($Test2);
	$Test->HienThi();
	
	
?>
<?php
$string = '{
   "cod":"200",
   "message":1.2566,
   "cnt":7,
   "list":[
      {
         "dt":1553936400,
         "main":{
            "temp":30.77,
            "temp_min":27.19,
            "temp_max":30.77,
            "pressure":1007.55,
            "sea_level":1007.55,
            "grnd_level":999.37,
            "humidity":83,
            "temp_kf":3.58
         },
         "weather":[
            {
               "id":500,
               "main":"Rain",
               "description":"light rain",
               "icon":"10d"
            }
         ],
         "clouds":{
            "all":76
         },
         "wind":{
            "speed":3.53,
            "deg":141.504
         },
         "rain":{
            "3h":0.02
         },
         "sys":{
            "pod":"d"
         },
         "dt_txt":"2019-03-30 09:00:00"
      },
      {
         "dt":1553947200,
         "main":{
            "temp":27.98,
            "temp_min":25.6,
            "temp_max":27.98,
            "pressure":1008.91,
            "sea_level":1008.91,
            "grnd_level":1000.78,
            "humidity":83,
            "temp_kf":2.38
         },
         "weather":[
            {
               "id":500,
               "main":"Rain",
               "description":"light rain",
               "icon":"10n"
            }
         ],
         "clouds":{
            "all":64
         },
         "wind":{
            "speed":4.47,
            "deg":143.008
         },
         "rain":{
            "3h":0.01
         },
         "sys":{
            "pod":"n"
         },
         "dt_txt":"2019-03-30 12:00:00"
      },
      {
         "dt":1553958000,
         "main":{
            "temp":24.75,
            "temp_min":23.56,
            "temp_max":24.75,
            "pressure":1011.13,
            "sea_level":1011.13,
            "grnd_level":1002.98,
            "humidity":90,
            "temp_kf":1.19
         },
         "weather":[
            {
               "id":500,
               "main":"Rain",
               "description":"light rain",
               "icon":"10n"
            }
         ],
         "clouds":{
            "all":92
         },
         "wind":{
            "speed":4.31,
            "deg":137.504
         },
         "rain":{
            "3h":0.13
         },
         "sys":{
            "pod":"n"
         },
         "dt_txt":"2019-03-30 15:00:00"
      },
      {
         "dt":1553968800,
         "main":{
            "temp":23.06,
            "temp_min":23.06,
            "temp_max":23.06,
            "pressure":1010.64,
            "sea_level":1010.64,
            "grnd_level":1002.43,
            "humidity":96,
            "temp_kf":0
         },
         "weather":[
            {
               "id":500,
               "main":"Rain",
               "description":"light rain",
               "icon":"10n"
            }
         ],
         "clouds":{
            "all":92
         },
         "wind":{
            "speed":2.41,
            "deg":124.502
         },
         "rain":{
            "3h":0.29
         },
         "sys":{
            "pod":"n"
         },
         "dt_txt":"2019-03-30 18:00:00"
      },
      {
         "dt":1553979600,
         "main":{
            "temp":23.11,
            "temp_min":23.11,
            "temp_max":23.11,
            "pressure":1010.04,
            "sea_level":1010.04,
            "grnd_level":1001.92,
            "humidity":97,
            "temp_kf":0
         },
         "weather":[
            {
               "id":500,
               "main":"Rain",
               "description":"light rain",
               "icon":"10n"
            }
         ],
         "clouds":{
            "all":92
         },
         "wind":{
            "speed":2.16,
            "deg":110.005
         },
         "rain":{
            "3h":0.37
         },
         "sys":{
            "pod":"n"
         },
         "dt_txt":"2019-03-30 21:00:00"
      },
      {
         "dt":1553990400,
         "main":{
            "temp":23.51,
            "temp_min":23.51,
            "temp_max":23.51,
            "pressure":1011.91,
            "sea_level":1011.91,
            "grnd_level":1003.58,
            "humidity":97,
            "temp_kf":0
         },
         "weather":[
            {
               "id":500,
               "main":"Rain",
               "description":"light rain",
               "icon":"10d"
            }
         ],
         "clouds":{
            "all":92
         },
         "wind":{
            "speed":2.36,
            "deg":123.501
         },
         "rain":{
            "3h":0.58
         },
         "sys":{
            "pod":"d"
         },
         "dt_txt":"2019-03-31 00:00:00"
      },
      {
         "dt":1554001200,
         "main":{
            "temp":26.73,
            "temp_min":26.73,
            "temp_max":26.73,
            "pressure":1013.16,
            "sea_level":1013.16,
            "grnd_level":1004.89,
            "humidity":92,
            "temp_kf":0
         },
         "weather":[
            {
               "id":500,
               "main":"Rain",
               "description":"light rain",
               "icon":"10d"
            }
         ],
         "clouds":{
            "all":24
         },
         "wind":{
            "speed":2.79,
            "deg":153.505
         },
         "rain":{
            "3h":0.17
         },
         "sys":{
            "pod":"d"
         },
         "dt_txt":"2019-03-31 03:00:00"
      }
   ],
   "city":{
      "id":1581130,
      "name":"Hanoi",
      "coord":{
         "lat":21.0292,
         "lon":105.8525
      },
      "country":"VN",
      "population":1431270
   }
}';
echo "<pre>";
var_dump(json_decode($string, true));
echo "</pre>"


?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<style>
	h1 {
		margin: auto; text-align: center; animation: doimau 7s infinite;
	}

	@keyframes doimau {
		0% {color: #F90F00};
		40% {color: #0DE018};
		80% {color: #241DCF};
		100% {color: #CD0DE0};
	}
	p {
  text-align: center;
  font-size: 60px;
  margin-top: 0px;
}
</style>
</head>

<body>
<?php 
	echo "<h1> Count Down đến tết</h1>";
	date_default_timezone_set('Asia/Ho_Chi_Minh');
	// lấy thời gian hiện tại (s):
	$TimeNow = strtotime("now");
	// set thời gian đến tết (s)
	$TimeStamp = mktime(0,0,0,2,05,2019); // Giờ phút giây Tháng ngày năm
	// tính khoảng cách (s)
	$distance = $TimeStamp - $TimeNow;
	// lấy giây / 86400 ra ngày. làm tròn số bằng floor
	$day = floor($distance/86400);
	// lấy số dư còn lại chia cho 3600 ra giờ
	$gio = floor(($distance % 86400) / 3600);
	// lấy số dư còn lại chia cho 60 ra phút
	$phut = floor((($distance % 86400) % 3600) / 60);
	// số dư còn lại là giây.
	$giay = ((($distance % 86400) % 3600) % 60);
	if($distance < 0)
		echo "<h1> HAPPY NEW YEAR </H1>";
	else echo "<h1>Còn: ".$day." ngày ".$gio." Giờ ".$phut ." Phút ".$giay. " Giây là đến tết kỷ Hợi 2019"
?>
<hr>
<h2>js :</h2>
<p id="demo"></p>

<script>
// Set the date we're counting down to
var countDownDate = new Date("February 5, 2019 0:0:0").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

    // Get todays date and time
    var now = new Date().getTime();
    
    // Find the distance between now and the count down date
    var distance = countDownDate - now;
    
    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
    // Output the result in an element with id="demo"
    document.getElementById("demo").innerHTML = days + "d " + hours + "h "
    + minutes + "m " + seconds + "s ";
    
    // If the count down is over, write some text 
    if (distance < 0) {
        clearInterval(x);
        document.getElementById("demo").innerHTML = "EXPIRED";
    }
}, 1000);
</script>



</body>
</html>